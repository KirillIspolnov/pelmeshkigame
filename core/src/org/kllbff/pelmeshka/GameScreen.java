package org.kllbff.pelmeshka;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;

import java.util.Iterator;

import static com.badlogic.gdx.graphics.GL20.GL_COLOR_BUFFER_BIT;

public class GameScreen implements Screen {
    private static final int WINDOW_WIDTH = 800;
    private static final int WINDOW_HEIGHT = 480;
    private static final int FACE_WIDTH = 85;
    private static final int FACE_HEIGHT = 64;
    private static final int PELMESHKA_SIZE = 64;

    private final PelmeshkiGame game;

    private Texture pelmeshkaImage;
    private Texture faceImage;
    private Sound omnomSound;
    private Music themeMusic;
    private OrthographicCamera camera;
    private Rectangle face;
    private Array<Rectangle> pelmeshki;
    private long lastDropTime;
    private int dropsGathered, dropsMissed;
    private long interval = 2000L;
    private Texture bg;

    public GameScreen(final PelmeshkiGame gam) {
        this.game = gam;

        pelmeshkaImage = new Texture(Gdx.files.internal("pelmeshka.png"));
        faceImage = new Texture(Gdx.files.internal("face.png"));

        omnomSound = Gdx.audio.newSound(Gdx.files.internal("omnomnom_15.mp3"));
        themeMusic = Gdx.audio.newMusic(Gdx.files.internal("main_theme.mp3"));
        themeMusic.setLooping(true);

        camera = new OrthographicCamera();
        camera.setToOrtho(false, WINDOW_WIDTH, WINDOW_HEIGHT);

        face = new Rectangle();
        face.x = WINDOW_WIDTH / 2 - FACE_WIDTH / 2;
        face.y = 0;

        face.width = FACE_WIDTH;
        face.height = FACE_HEIGHT;

        pelmeshki = new Array<>();
        //spawnRaindrop();
        bg = new Texture(Gdx.files.internal("bg.png"));
    }

    private void spawnRaindrop() {
        Rectangle raindrop = new Rectangle();
        raindrop.x = MathUtils.random(FACE_WIDTH, WINDOW_WIDTH - PELMESHKA_SIZE);
        raindrop.y = WINDOW_HEIGHT;
        raindrop.width = PELMESHKA_SIZE;
        raindrop.height = PELMESHKA_SIZE;
        pelmeshki.add(raindrop);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL_COLOR_BUFFER_BIT);

        camera.update();
        game.batch.setProjectionMatrix(camera.combined);

        game.batch.begin();

        game.batch.draw(bg, 0, 0, WINDOW_WIDTH, 500);
        game.font.draw(game.batch, "Пельмешек съедено: " + dropsGathered, 0, WINDOW_HEIGHT);
        game.font.draw(game.batch, "Пельмешек пропущено: " + dropsMissed, 0, WINDOW_HEIGHT - 50);
        game.batch.draw(faceImage, face.x, face.y);

        for (Rectangle raindrop : pelmeshki) {
            game.batch.draw(pelmeshkaImage, raindrop.x, raindrop.y);
        }

        game.batch.end();

        if (Gdx.input.isTouched()) {
            Vector3 touchPos = new Vector3();
            touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
            camera.unproject(touchPos);
            face.x = touchPos.x - FACE_WIDTH / 2;
        }

        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            face.x -= 200 * Gdx.graphics.getDeltaTime();
        }

        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            face.x += 200 * Gdx.graphics.getDeltaTime();
        }

        if (face.x < 0) {
            face.x = 0;
        }
        if (face.x > WINDOW_WIDTH - FACE_WIDTH) {
            face.x = WINDOW_WIDTH - FACE_WIDTH;
        }

        if (System.currentTimeMillis() - lastDropTime > interval) {
            lastDropTime = System.currentTimeMillis();
            spawnRaindrop();
        }

        Iterator<Rectangle> iter = pelmeshki.iterator();
        while (iter.hasNext()) {
            Rectangle raindrop = iter.next();
            raindrop.y -= 200 * Gdx.graphics.getDeltaTime();
            if (raindrop.y + FACE_WIDTH < 0) {
                iter.remove();
                dropsMissed++;
            } else if (overlaps(raindrop, face)) {
                dropsGathered++;

                if(dropsGathered % 50 == 0) {
                    interval /= 1.25;
                }

                omnomSound.stop();
                omnomSound.play();
                iter.remove();
            }
        }
    }

    private boolean overlaps(Rectangle raindrop, Rectangle bucket) {
        return raindrop.x > bucket.x + 20 && raindrop.y < bucket.y + 16 &&
               raindrop.x < bucket.x + bucket.width;
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void show() {
        // start the playback of the background music
        // when the screen is shown
        themeMusic.play();
    }

    @Override
    public void hide() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
        pelmeshkaImage.dispose();
        faceImage.dispose();
        omnomSound.dispose();
        themeMusic.dispose();
    }

}
